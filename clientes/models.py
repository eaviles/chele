from django.db import models

# Create your models here.
class Clientes(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    email = models.EmailField(max_length=50, null=True, blank=True)
    telefono = models.CharField(max_length=20, null=True, blank=True)


    def __unicode__(self):
        return "{0} {1}".format(self.nombre, self.apellido)
