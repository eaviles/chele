from django.db import models

# Create your models here.
class Empresa(models.Model):
    nombre = models.CharField(max_length=200)
    telefono = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(max_length=50, null=True, blank=True)
    direcction = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.nombre


class Sucursal(models.Model):
    empresa = models.ForeignKey(Empresa)
    nombre = models.CharField(max_length=200)
    telefono = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(max_length=50, null=True, blank=True)
    direcction = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return "{0} - {1}".format(self.empresa, self.nombre)


class Empleado(models.Model):
    empresa = models.ForeignKey(Empresa)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    carnet = models.CharField(max_length=50)
    cargo = models.CharField(max_length=100)

    def __unicode__(self):
        return "{0} - {1}".format(self.empresa, self.nombre)