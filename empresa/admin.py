from django.contrib import admin
from empresa.models import Empresa, Sucursal, Empleado
# Register your models here.

@admin.register(Empresa, Sucursal, Empleado)
class EmpresaAdmin(admin.ModelAdmin):
    pass