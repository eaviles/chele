from django.db import models

# Create your models here.
class Proveedor(models.Model):
    nombre = models.CharField(max_length=150)

    def __unicode__(self):
        return self.nombre


class Productos(models.Model):
    nombre = models.CharField(max_length=150)
    size = models.CharField(max_length=50)
    proveedor = models.ForeignKey(Proveedor)

    def __unicode__(self):
        return self.nombre
